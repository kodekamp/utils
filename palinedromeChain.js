function palindrome(num) {
  let rev = parseInt(
    num
      .toString()
      .split("")
      .reverse()
      .join("")
  );

  return num === rev ? rev : palindrome(num + rev);
}

palindrome(87);
