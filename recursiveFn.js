module.exports = {
  /* 
    This function takes a number and continualy iterates over it until it becomes a palindorome
  */
  palindromeNum(num) {
    let rev = parseInt(
      num
        .toString()
        .split("")
        .reverse()
        .join("")
    );

    return num === rev ? rev : palindrome(num + rev);
  },

  /* 
    This function takes a number and sums from 1 - num passed in.
  */
  sumTo(num) {
    if (num === 1) return num;

    return num + sumTo(num - 1);
  },

  /* 
    This function returns the factorial of a number: n + (n-1)
  */
  factorial(num) {
    if (num < 0) return;
    if (num === 1) return num;

    return num * factorial(num - 1);
  },

  /* 
    This function returns the fibonacci series of a given num

    NOTE: for large numbers ie: fib(77), the process is slow. consider using a forloop
  */
  fib(num) {
    return num <= 1 ? num : fib(num - 1) + fib(num - 2);
  }
};
